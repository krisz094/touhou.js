
const FPS = 50
const CANVAS_WIDTH = 800
const CANVAS_HEIGHT = 600
const BULLET_SIZE = 10
const HITBOX_SIZE = 8
var TIME_BETWEEN_SHOTS = 80
var SPRITE = new Image()
SPRITE.src = './images/chen.png'

// SET UP SHOT TYPES
const SHOT_SIMPLE = 1
const SHOT_SPREADTRIPLE = 2
const SHOT_CONCENTRATEDTRIPLE = 3
var CURRENT_SHOT_TYPE = SHOT_SIMPLE

// SET UP SPEEDS
const TO_MOVE_SPEED_SLOW = 3 * 30 / FPS
const TO_MOVE_SPEED_FAST = 10 * 30 / FPS
const BULLET_TRAVEL_SPEED = 18 * 30 / FPS
var TO_MOVE = TO_MOVE_SPEED_FAST
var pressed = {
    up: false,
    down: false,
    left: false,
    right: false,
    space: false
}
var canvas = document.getElementById('gamecanv')
canvas.width = 800
canvas.height = 600
var shots = []
document.onkeydown = function (e) {
    e.preventDefault()
    switch (e.key) {
        case 'ArrowUp':
            pressed.up = true
            break
        case 'ArrowDown':
            pressed.down = true
            break
        case 'ArrowRight':
            pressed.right = true
            break
        case 'ArrowLeft':
            pressed.left = true
            break
        case ' ':
            startShooting()
            break
        case 'Shift':
            TO_MOVE = TO_MOVE_SPEED_SLOW
            break
        case '1':
            CURRENT_SHOT_TYPE = SHOT_SIMPLE
            break
        case '2':
            CURRENT_SHOT_TYPE = SHOT_SPREADTRIPLE
            break
        case '3':
            CURRENT_SHOT_TYPE = SHOT_CONCENTRATEDTRIPLE
            break
    }
    //console.log(e.key)
}
document.onkeyup = function (e) {
    e.preventDefault()
    switch (e.key) {
        case 'ArrowUp':
            pressed.up = false
            break
        case 'ArrowDown':
            pressed.down = false
            break
        case 'ArrowRight':
            pressed.right = false
            break
        case 'ArrowLeft':
            pressed.left = false
            break
        case ' ':
            stopShooting()
            break
        case 'Shift':
            TO_MOVE = TO_MOVE_SPEED_FAST
            break

    }
    //console.log(pressed)

}
/**
 * @returns {CanvasRenderingContext2D}
 */
function getctx() {
    return canvas.getContext("2d")
}
var ctx = getctx()

var pos = {
    x: CANVAS_WIDTH / 2,
    y: CANVAS_HEIGHT - 50
}
class Bullet {
    constructor(x, y, vectX, vectY) {
        this.x = x
        this.y = y
        this.vectX = vectX
        this.vectY = vectY
    }
    iterate() {
        this.x += this.vectX
        this.y += this.vectY
    }
}
class Shot {
    constructor(x, y) {
        this.bullets = []
        this.x = x
        this.y = y
    }
    iterate() {
        this.y -= BULLET_TRAVEL_SPEED
        this.bullets.forEach(bullet => {
            bullet.iterate()
        })
    }
}
class BasicShot extends Shot {
    constructor(x, y) {
        super(x, y)
        this.bullets = [new Bullet(x, y, 0, -BULLET_TRAVEL_SPEED)]
    }
}
class SpreadTripleShot extends Shot {
    constructor(x, y) {
        super(x, y)
        this.bullets = [
            new Bullet(x, y, -BULLET_TRAVEL_SPEED / 3, -BULLET_TRAVEL_SPEED),
            new Bullet(x, y, 0, -BULLET_TRAVEL_SPEED),
            new Bullet(x, y, BULLET_TRAVEL_SPEED / 3, -BULLET_TRAVEL_SPEED)
        ]
    }
}
class ConcentratedTripleShot extends Shot {
    constructor(x, y) {
        super(x, y)
        this.bullets = [
            new Bullet(x - 12, y, 0, -BULLET_TRAVEL_SPEED),
            new Bullet(x, y, 0, -BULLET_TRAVEL_SPEED),
            new Bullet(x + 12, y, 0, -BULLET_TRAVEL_SPEED)
        ]
    }
}
var shootInterval;

function shoot() {
    switch (CURRENT_SHOT_TYPE) {
        case SHOT_SIMPLE:
            var newShot = new BasicShot(pos.x, pos.y)
            break
        case SHOT_SPREADTRIPLE:
            var newShot = new SpreadTripleShot(pos.x, pos.y)
            break
        case SHOT_CONCENTRATEDTRIPLE:
            var newShot = new ConcentratedTripleShot(pos.x, pos.y)
            break

    }
    shots.push(newShot)
}

function startShooting() {

    if (!shootInterval) {
        shoot()
        console.log('elkezdtem')
        shootInterval = setInterval(() => {
            shoot()
        }, TIME_BETWEEN_SHOTS)
    }
}

function stopShooting() {
    if (shootInterval) {
        clearInterval(shootInterval)
        shootInterval = null
    }
}

setInterval(() => {
    if (pressed.up) {
        pos.y -= TO_MOVE
    }
    if (pressed.down) {
        pos.y += TO_MOVE
    }
    if (pressed.left) {
        pos.x -= TO_MOVE
    }
    if (pressed.right) {
        pos.x += TO_MOVE
    }
    if (pos.y < 0) {
        pos.y = 0
    }
    if (pos.x < 0) {
        pos.x = 0
    }
    if (pos.y > CANVAS_HEIGHT - HITBOX_SIZE) {
        pos.y = CANVAS_HEIGHT - HITBOX_SIZE
    }
    if (pos.x > CANVAS_WIDTH - HITBOX_SIZE) {
        pos.x = CANVAS_WIDTH - HITBOX_SIZE
    }

    shots.forEach((shot, idx) => {
        shot.iterate()
        if (shot.y < 0)
            shots.splice(idx, 1)
    })
}, 1000 / FPS)

setInterval(function () {
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    shots.forEach(shot => {
        ctx.fillStyle = 'red'
        shot.bullets.forEach(bullet => {
            ctx.fillRect(bullet.x, bullet.y, BULLET_SIZE, BULLET_SIZE)
        })
    })
    ctx.drawImage(SPRITE, pos.x - 9, pos.y - 20)
    ctx.fillStyle = 'white'
    ctx.strokeStyle = 'black'
    ctx.fillRect(pos.x, pos.y, HITBOX_SIZE, HITBOX_SIZE)
    ctx.strokeRect(pos.x, pos.y, HITBOX_SIZE, HITBOX_SIZE)
    shots.forEach(shot => {
        ctx.fillStyle = 'red'
        shot.bullets.forEach(bullet => {
            ctx.fillRect(bullet.x, bullet.y, BULLET_SIZE, BULLET_SIZE)
        })
    })

}, 1000 / FPS)